﻿" Gvim settings file 
" goes into: ~/.gvimrc 

" Disable Gvim toolbar at the top
set guioptions-=T
" Disable menu
" set guioptions-=m
" Disable scrollbars
set guioptions-=l
set guioptions-=r
set guioptions-=b
" And Remove scrollbars on split
set guioptions-=L

" Use CTRL-X, CTRL-C, CTRL-V and CTRL-Z for clipboard
source $VIMRUNTIME/mswin.vim


set gfn=Monospace\ 11 
if has("win32")
    " Start maximized
    au GUIEnter * simalt ~x
    set guifont=Consolas:h11
endif

" Hightlight the foldings differently 
hi Folded term=standout ctermbg=white ctermfg=Blue guibg=yellow guifg=Black 
" Highlight comments differently 
highlight comment  ctermfg=green 
" Hightlight directory explorer differently 
hi Directory term=standout ctermbg=yellow ctermfg=LightBlue guibg=orange guifg=white 

"Set our colorscheme 
" colorscheme blackboard 
colorscheme darkblue
" Got this scheme (as well as python highlighter) from

" http://concisionandconcinnity.blogspot.com/2009/07/vim-part-i-improved-python-syntax.html
