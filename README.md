# About #

This is my personal centralized cross-platform vim configuration and plugin git
repo. This allows for sharing of vim/gvim environments between different
machines (some unix, some windows), while keeping consistent set of settings.
The setup is meant to be as simple as possible, but not simpler. I've decided
to make this repo public in case anyone else may find it useful. Furthermore,
issues can be reported in the issue tracker.

# Setup #

1. Setup symlinks
2. Initialize plugins
3. Installing new plugins
4. Removing plugins

## Symlinks ##

### Windows ###

Assume we cloned the repository into "C:\Users\john\configurations\vimfiles". 
Then from your home directory (e.g. ``C:\Users\john``):

    C:\Users\john> mklink /D vimfiles C:\Users\john\configurations\vimfiles
    C:\Users\john> mklink .vimrc C:\Users\john\configurations\vimfiles\.vimrc
    C:\Users\john> mklink .gvimrc C:\Users\john\configurations\vimfiles\.gvimrc

### Linux ###

Create symlinks to folder ``.vim`` and files:

    ln -s /path-to/vimfiles ~/.vim
    ln -s /path-to/vimfiles/.vimrc ~/.vimrc
    ln -s /path-to/vimfiles/.gvimrc ~/.gvimrc

## Initialize plugins ##

    git submodule init
    git submodule update

## Installing new plugins ##

### From zip ###

Simply unzip them into the "bundles" folder, e.g.:

   bundle/pluginFolder

### From Git ###

These plugins are stored a git submodules, in the `bundle` directory, and are
automatically loaded by a special plugin called `pathogen` which is already
present in the autoload folder.

To add new plugins simply add them as a submodule to the "bundles" folder. 

#### Example ####

To add plugin such as `tabular` (https://github.com/godlygeek/tabular), we first 
strip out the "https" and use just "http", and then add it as a submodule to our `bundle`
directory:

    git submodule add http://github.com/godlygeek/tabular bundle/tabular
    git commit -a -m "Added tabular plugin"
    
#### Dirty status ####

Sometimes when you do `git status` a submodule will show up as dirty. To fix this
go into `.gitmodules` file and add `ignore = dirty` attribute to the affected plugin. E.g.:

	[submodule "bundle/nerdtree"]
	path = bundle/nerdtree
	url = http://github.com/vim-scripts/The-NERD-tree.git
	ignore = dirty


## Removing Plugins ##

To remove a plugin (stored as Git submodule) do the following:

1. Delete a relevant section from `.gitmodules` file
2. Delete relevant section from `.git/config` file
3. Run `git rm --cached bundle/plugin_name`, where `plugin_name` is the name of the plugin/module.
4. Commit and delete untracked files.
