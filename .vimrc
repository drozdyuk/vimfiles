" Filetype detection off when loading pathogen:
filetype off
call pathogen#runtime_append_all_bundles()
call pathogen#helptags()

" " This must be first, because it changes other options as a side effect.
set nocompatible


set history=50

" Enable the supid backspace in windows
set bs=2

" Enable hidden buffers (switching between unsaved buffers)
set hidden

" Navigate buffers with Ctrl+N and Ctrl+P
map <C-P> :bp<cr>
map <C-N> :bn<cr>

"Enable code folding - for python methods
set foldmethod=indent " 'za' inside a method opens or closes it
set foldlevel=99


set encoding=utf8

" Look and feel
set showmode
set showcmd
set ruler

" Paste mode - allows pasting without stupid auto indenting
set pastetoggle=<F2>


" Recognize all types of line endings.
set fileformats=unix,mac,dos

set wildignore=*.o,*.obj,*.bak,*.pyc

" Match the start/end tags in other languages like html etc...
" source $VIMRUNTIME/macros/matchit.vim
source $VIMRUNTIME/macros/matchit.vim

" IMPORTANT: grep will sometimes skip displaying the file name if you
" search in a singe file. This will confuse Latex-Suite. Set your grep
" program to always generate a file-name.
set grepprg=grep\ -nH\ $*

" OPTIONAL: Starting with Vim 7, the filetype of empty .tex files defaults to
" 'plaintex' instead of 'tex', which results in vim-latex not being loaded.
" The following changes the default filetype back to 'tex':
let g:tex_flavor='latex'

syntax on


" Highlight the search term, and make space toggle hightlights
set hlsearch
function! HLStoggle()
    let &hls=&hls==0
    set hlsearch?
endfunction
nnoremap <silent> <Space> :call HLStoggle()<Bar>:echo<CR>

" Only syntax highlight first 100 columns else vim will be really slow!
set synmaxcol=100


filetype on
" Enable file type detection.
" Also load indent files, to automatically do language-dependent indenting.
filetype plugin indent on

" Hightlight the foldings differently
hi Folded term=standout ctermbg=Black ctermfg=Blue guibg=Black guifg=Black
" Highlight comments differently
highlight comment  ctermfg=green
" Hightlight directory explorer differently
hi Directory term=standout ctermbg=Black ctermfg=LightBlue guibg=Black guifg=Black


" save and restore visual settings (like folds)
" au BufWinLeave * mkview
" au BufWinEnter * silent loadview


" Associate .sty files with latex classes
au BufRead,BufNewFile *.sty set filetype=tex


" highligh current line
set cursorline

" dont make it look like there are line breaks,where there arent'
set nowrap

" line num
set nu

" use indents of 2 spaces, and have them copied down lines:
set shiftwidth=4
set softtabstop=4
set tabstop=4
set shiftround
set autoindent
set expandtab

set incsearch

set ignorecase
set smartcase

set formatoptions-=t
set textwidth=79



"Corret common typos
abbreviate teh the
abbreviate spolier spoiler
abbreviate doign doing

" have lh keys wrap between lines
set whichwrap=h,l,~,[,]

noremap �Space� �PageDown�
noremap �BS� �PageUp�


