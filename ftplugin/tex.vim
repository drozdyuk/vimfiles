" How long each line should be before breaking
set textwidth=80
" Automatic line wrapping
set fo+=t

" Tell vim-latex to compile to pdf
let g:Tex_DefaultTargetFormat='pdf'
" Compile using xelatex
let g:Tex_CompileRule_pdf = 'xelatex --interaction=nonstopmode $*'
